/* eslint-disable @typescript-eslint/no-unused-vars */
import { Introduce } from '@containers/Introduce';
import React, { useEffect, useRef, useState } from 'react';
import styled from 'styled-components';
import useMedia from 'use-media';

import SEOHead, { SEOHeadProps } from '../SEOHead';
import { FullPage, Slide } from './react_fullpage_src';

const Container = styled.div`
  width: 100%;
  margin: 0 auto;
  padding: 0 24px;
  text-align: center;
`;

const Wrapper = styled.div``;

const HeaderWrapper = styled.div`
  top: 0;
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const MainWrapper = styled.div`
  @media (max-width: 600px) {
    overflow-x: hidden;
    overflow-y: scroll;
  }

  width: calc(100vw - (100vw - 100%));
  height: 100vh; //${({ height }: { height: number }) => `${height}px`};
  scroll-snap-type: y mandatory;
  scroll-padding: 20px;

  .child {
    height: 100%;
    scroll-snap-align: center;
    display: flex;
    justify-content: center;
  }

  .child :nth-child(odd) {
  }

  .child :nth-child(even) {
  }
`;

export type LayoutProps = {
  headProps?: SEOHeadProps;
};

export const Layout: React.FC<LayoutProps> = ({ headProps, ...props }) => {
  const [isHeader, setIsHeader] = useState(true);
  const [direction, setDirection] = useState(false);
  const [currentPage, setCurrentPage] = useState(0);
  const [height, setHeight] = useState(0);
  const fullPageRef = useRef(null);
  const isMobile = useMedia({ maxWidth: 600 });

  const setPage = (number) => {
    setCurrentPage(number);
    if (fullPageRef != null && fullPageRef.current != null) fullPageRef.current.scrollToSlide(number);
  };

  const afterChangePage = (data) => {
    setCurrentPage(data.to);
  };

  // const isWide = useMedia({ maxWidth: 1545 });

  useEffect(() => {
    setHeight(window.innerHeight);

    let touchPos = 0;
    // store the touching position at the start of each touch
    document.body.ontouchstart = function (e) {
      touchPos = e.changedTouches[0].clientY;
    };

    // detect wether the "old" touchPos is
    // greater or smaller than the newTouchPos
    document.body.ontouchmove = function (e) {
      const newTouchPos = e.changedTouches[0].clientY;
      if (newTouchPos > touchPos) {
        setDirection(true);
      }
      if (newTouchPos < touchPos) {
        setDirection(false);
      }
    };
  }, []);

  return (
    <Wrapper {...props}>
      <SEOHead {...headProps} />

      {/* Header */}
      <HeaderWrapper>{/* <NavBar changePage={changePage} /> */}</HeaderWrapper>
      {/* Layout content */}
      <MainWrapper height={height} id="main-wrapper">
        {isMobile ? (
          <>
            <Introduce setIsHeader={setIsHeader} />
          </>
        ) : (
          <FullPage afterChange={afterChangePage} ref={fullPageRef} initialSlide={currentPage}>
            <Slide>
              <Introduce setIsHeader={setIsHeader} />
            </Slide>
          </FullPage>
        )}
      </MainWrapper>
    </Wrapper>
  );
};

export { Container as LayoutContainer };
export default Layout;
