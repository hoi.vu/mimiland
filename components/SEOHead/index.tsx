import { APP_DESCRIPTION, APP_NAME, APP_SITE_URL } from '@config/constants';
import NextHead from 'next/head';
import React from 'react';

export type SEOHeadProps = {
  seoTitle?: string;
  seoDescription?: string;
  seoURL?: string;
  seoImage?: string;
  favicon?: string;
};

/**
 * SEO Head component
 */
export const SEOHead: React.FC<SEOHeadProps> = ({
  seoTitle = APP_NAME,
  seoDescription = APP_DESCRIPTION,
  seoURL = APP_SITE_URL,
  seoImage = '/assets/images/dpet_icon.png',
  favicon = '/assets/images/dpet_icon.png',
  children,
}) => {
  return (
    <NextHead>
      <meta charSet="utf-8" />
      <title>{seoTitle}</title>
      <meta name="description" content={seoDescription} />
      <meta
        name="google-signin-client_id"
        content="441420406476-n01j67g751qscpf5dimvmrn02qr64j0t.apps.googleusercontent.com"
      ></meta>
      {/* <meta
        name="viewport"
        content="width=device-width,initial-scale=1,shrink-to-fit=no,maximum-scale=1,user-scalable=no"
      /> */}
      <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

      <link rel="shortcut icon" href={favicon} />
      <link rel="icon" href={favicon} />

      <meta name="twitter:site" content={seoURL} />
      <meta name="twitter:card" content="summary_large_image" />
      <meta name="twitter:image" content={seoImage} />

      <meta property="og:title" content={seoTitle} />
      <meta property="og:description" content={seoDescription} />
      <meta property="og:url" content={seoURL} />
      <meta property="og:image" content={seoImage} />
      {/* Additional content */}
      {children}
    </NextHead>
  );
};

export default SEOHead;
