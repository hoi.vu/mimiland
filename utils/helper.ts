export const getImagePath = (name: string) => `/assets/images/${name}`;
export const getElementImagePath = (ele: number) => {
  let imgName = '';
  switch (ele) {
    case 0:
      imgName = 'ele_wind.png';
      break;
    case 1:
      imgName = 'ele_earth.png';
      break;
    case 2:
      imgName = 'ele_water.png';
      break;
    case 3:
      imgName = 'ele_fire.png';
      break;
    case 4:
      imgName = 'ele_dark.png';
      break;
    case 5:
      imgName = 'ele_light.png';
      break;
  }

  return getImagePath(`Elements/${imgName}`);
};

export const showLog = (log: string) => {
  alert(log);
};

export const openInNewTab = (url: string) => {
  window.open(url, '_blank');
  return;
};
