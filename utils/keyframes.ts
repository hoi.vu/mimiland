/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import { keyframes } from 'styled-components';

export const zoomBack = keyframes`
 0% {
    opacity: 0;
    transform: translateZ(0) scale(1);
  }
  50% {
    opacity: 1;
    transform: translateZ(-4em) scale(1.2);
  }
  100% {
    opacity: 1;
    transform: translateZ(0) scale(1);
  }
`;

export const moveUp = keyframes`
 0% {
    opacity: 0.7;
    transform: translateY(50px);
  }
 
  100% {
    opacity: 1;
    transform: translateY(0px);
  }
`;

export const moveDown = keyframes`
 0% {
    opacity: 0.7;
    transform: translateY(-50px);
  }
 
  100% {
    opacity: 1;
    transform: translateY(0px);
  }
`;

export const textClip = keyframes`
  from {
    opacity: 0.7;
    clip-path: polygon(0% 100%, 100% 100%, 100% 100%, 0% 100%);
  }
  to {
    opacity: 1;

    clip-path: polygon(0 0, 100% 0, 100% 100%, 0 100%);
  }
`;

export const moveUpBig = keyframes`
 0% {
    opacity: 0.7;
    transform: translateY(50px);
  }
 
  100% {
    opacity: 1;
    transform: translateY(0px);
  }
`;

export const moveIn = keyframes`
 0% {
    opacity: 0;
    transform: translateX(-100px);
  }
 
  100% {
    opacity: 1;
    transform: translateX(0px);
  }
`;

export const zoomOut = keyframes`
 0% {
    opacity: 0;
    transform: translateZ(0) scale(0.8);
  }
 
  100% {
    opacity: 1;
    transform: translateZ(-4em) scale(1);
  }
`;

export const zoom = keyframes`
 0% {
    transform:  scale(1);
  }
 
  50% {
    transform: scale(1.1);
  }
  100% {
    transform:  scale(1);
  }
`;

export const rotate3D = keyframes`
 0% {
    transform: rotate3d(0, 1, 0, 0deg);
  }
 
 
  100% {
    transform: rotate3d(0, 1, 0, 360deg);
  }
`;

export const rotate = keyframes`
 0% {
    transform: rotate( 0deg);
  }
 
  100% {
    transform: rotate( 360deg);
  }
`;

export const flyBottom = keyframes`
0% {
   transform: translateX(20vw) translateY(50vh) rotate(-20deg);
   opacity: 1;
  
 }

 100% {
   transform: translateX(100vw) translateY(-60vh) rotate(-20deg);
   opacity: 0.4;
 }
`;
export const flyinit = keyframes`
 100% {
   bottom:-400px;
right: 50vw;
 }
`;

export const flyTop = keyframes`
0% {
   transform: translateX(-40vw) translateY(30vh) rotate(-20deg);
   opacity: 1;
 }

 100% {
   transform: translateX(40vw) translateY(-50vh) rotate(-20deg);
   opacity: 0.2;
 }
`;

export const flareMoveUp = () => keyframes`
0% {
   transform:  translateY(0) rotate3d(0, 1, 0, ${Math.floor(Math.random() * 60)}deg);
   opacity: 0;
 }
 30% {
   transform:  translateY(0) rotate3d(0, 1, 0, ${Math.floor(Math.random() * 60)}deg);
   opacity: 1;
 }
 100% {
   transform:  translateY(-200px) rotate3d(0, 1, 0, 90deg) ;
   opacity: 0;
 }
`;

export const flareMoveUp1 = keyframes`
0% {
   transform:  translateY(30px) rotate3d(0, 1, 0, 70deg);
   opacity: 0;
 }

 30% {
   transform:  translateY(30px) rotate3d(0, 1, 0, 70deg);
   opacity: 1;
 }
 100% {
   transform:  translateY(-200px) rotate3d(0, 1, 0, 90deg) ;
   opacity: 0;
 }
`;

export const moveUpLarge = keyframes`
0% {
   transform:  translateY(400px) ;
   opacity: 1;
 }

 
 100% {
   transform:  translateY(-2333px)  ;
   opacity: 1;
 }
`;

export const flyMoveUp = keyframes`
0% {
   transform:  translateY(0px) ;
   opacity: 1;
 }
 40% {
   transform:  translateY(20px)  ;
   opacity: 1;
 }
 
 100% {
   transform:  translateY(0px)  ;
   opacity: 1;
 }
`;
