import styled from 'styled-components';

export const FixedContainer = styled.div`
  width: 100vw;
  position: fixed !important;
  top: 0;
  z-index: 9009;
`;

export const FixedContainer2 = styled.div`
  width: 100vw;
  position: fixed !important;
  top: 0;
  z-index: 9009;
  @media (max-width: 600px) {
    display: none;
  }
`;

export const AbsoluteContainer = styled.div`
  position: absolute;
  top: 0;
`;
export const RelativeContainer = styled.div`
  position: relative;
  top: 0;
`;
export const CenterWrapper = styled.div`
  display: flex;
  justify-content: center;
`;

export const CenterVehicleWrapper = styled.div`
  display: flex;
  align-items: center;
  flex-direction: column;
`;
export const SessionsWrapper = styled.div`
  display: flex;
  justify-content: center;
  width: 100%;
`;

export const Session = styled.div`
  max-width: 600px;
  width: 100%;
  padding: 0 10px;
  @media only screen and (min-device-width: 768px) and (max-device-width: 1024px) and (-webkit-min-device-pixel-ratio: 1) {
    margin-top: -30px;
  }
`;

export const Button = styled.button`
  background-color: #e12120;

  color: #ffffff;
  text-transform: capitalize;
  font-family: Signika-Light;
  padding: 20px;
  position: relative;
  overflow: hidden;
  border: none;
  & span {
    position: relative;
    z-index: 100;
  }
  & :hover ::after {
    left: 386px;
  }
  & ::after {
    position: absolute;
    left: -50px;
    top: -30px;
    content: '';
    width: 300px;
    height: 300px;
    transform: rotate(30deg);
    transition: all 0.5s;
    background-color: #ff2020;
    @media (max-width: 768px) {
      display: none;
    }
    & :hover {
      left: 386px;
    }
  }
`;
