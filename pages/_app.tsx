import './style.css';

import GlobalStyle from '@config/GlobalStyle';
import theme from '@config/theme';
import { AppProps } from 'next/app';
import React from 'react';
import { ThemeProvider } from 'styled-components';

function App({ Component, pageProps, err }: AppProps & { err: Error }): JSX.Element {
  return (
    <>
      <ThemeProvider theme={theme}>
        <GlobalStyle />
        <Component {...pageProps} err={err} />
      </ThemeProvider>
    </>
  );
}

export default App;
