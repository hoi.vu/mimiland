import Document, {
  DocumentContext,
  DocumentInitialProps,
  DocumentProps,
  Head,
  Html,
  Main,
  NextScript,
} from 'next/document';
import React from 'react';
import { ServerStyleSheet } from 'styled-components';

type Props = {
  lang: string;
  locale: string;
};

class AppDocument extends Document<Props & DocumentProps> {
  static async getInitialProps(ctx: DocumentContext): Promise<Props & DocumentInitialProps> {
    const sheet = new ServerStyleSheet();
    const originalRenderPage = ctx.renderPage;

    try {
      ctx.renderPage = () =>
        originalRenderPage({
          enhanceApp: (App) => (props) => sheet.collectStyles(<App {...props} />),
        });

      const initialProps = await Document.getInitialProps(ctx);

      const hasLocaleFromUrl = !!ctx.query?.locale;
      const locale = hasLocaleFromUrl ? (ctx.query?.locale as string) : 'en';
      const lang = locale.split('-')?.[0];

      return {
        ...initialProps,
        lang,
        locale,
        styles: (
          <>
            {initialProps.styles}
            {sheet.getStyleElement()}
          </>
        ),
      };
    } finally {
      sheet.seal();
    }
  }

  render(): JSX.Element {
    const { lang } = this.props;

    return (
      <Html lang={lang}>
        <Head />
        <body>
          <Main />
          <NextScript />
        </body>
      </Html>
    );
  }
}

export default AppDocument;
