module.exports = {
  serverRuntimeConfig: {},
  publicRuntimeConfig: {},
  pageExtensions: ['ts', 'tsx', 'mdx'],
  poweredByHeader: false,
  env: {},
  exportPathMap: async function (defaultPathMap, { dev, dir, outDir, distDir, buildId }) {
    return {
      '/ourteam': {
        page: '/ourteam',
      },
      '/': {
        page: '/',
      },
     
    };
  },

  images: {
    domains: ['localhost'],
    // next line is not required
    path: 'http://localhost:3000/',
  },
};
