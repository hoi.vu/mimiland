import React from 'react';
import styled from 'styled-components';

const Wrapper = styled.div`
  color: #fff;
`;

export const Container: React.FC = () => {
  return <Wrapper>Container</Wrapper>;
};

export default Container;
