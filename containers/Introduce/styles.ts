import { BG_NAME } from '@config/constants';
import { getImagePath } from '@utils/helper';
import styled from 'styled-components';

export const Container = styled.div`
  width: 100vw;
  height: 100vh;
  background: ${({ bgIndex }: { bgIndex: number }) => `url(${getImagePath(BG_NAME[bgIndex])}) no-repeat center`};
  background-size: 100% 100%;

  @media (max-width: 600px) {
    background-size: cover;
  }
  position: relative;
  z-index: 0;
  scroll-snap-align: center;
`;
