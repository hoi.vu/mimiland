import React, { useEffect } from 'react';
import { useInView } from 'react-hook-inview';
import styled from 'styled-components';

import { Container } from './styles';

interface Props {
  setIsHeader: (b: boolean) => void;
  setNavBar?: (id: string) => void;
}

export const LeftImg = styled.img`
  position: absolute;
  z-index: 0;
  top: 0px;
  left: -250px;
  max-height: 100%;

  @media (max-width: 600px) {
    top: 50px;
    height: 500px;
    left: -270px;
    display: none;
  }
`;

export const RightImg = styled.img`
  position: absolute;
  z-index: 0;
  top: 50px;
  right: -150px;
  max-height: 100%;

  @media (max-width: 600px) {
    top: 70px;
    right: -270px;
    height: 500px;
    display: none;
  }
`;

export const VideoPlayer = styled.video`
  position: absolute;
  object-fit: cover;
  width: 100%;
  height: 100%;
  /* padding: 10px; */

  /* display: flex; */
  /* justify-content: center; */
  /* align-items: center; */

  @media (max-width: 600px) {
    display: none;
  }
`;

export const Introduce: React.FC<Props> = (props) => {
  const [ref, isVisible] = useInView({
    threshold: 0.9,
  });

  useEffect(() => {
    props.setIsHeader?.(isVisible);
    // props.setNavBar?.('Introduce');
  }, [isVisible, props.setIsHeader]);

  return (
    <Container ref={ref} id="Introduce" className="Container child" bgIndex={1}>
      {/* <LeftImg src={getImagePath('pet_gr2.png')}></LeftImg>
      <RightImg src={getImagePath('pet_gr1.png')}></RightImg> */}
      <VideoPlayer playsInline autoPlay loop muted>
        <source src="/assets/movie.mp4" type="video/mp4"></source>
      </VideoPlayer>
    </Container>
  );
};
