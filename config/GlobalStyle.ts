import { moveUp } from '@utils/keyframes';
import { createGlobalStyle } from 'styled-components';

const GlobalStyle = createGlobalStyle`


  *, ::after, ::before {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
  }
  html, body {
    height: 100%;
    font-family: Signika-Light;
    font-size: 16px;
    line-height: 1.5;
    /* background-color: #050403; */
    color: #0C192D;
    /* overflow-x: hidden; */
    -ms-overflow-style: -ms-autohiding-scrollbar;
    -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
    -webkit-touch-callout: none;
    -webkit-text-size-adjust: 100%;
    -ms-text-size-adjust: 100%;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
    scroll-behavior: smooth;  

    -webkit-overflow-scrolling: touch;
 

  }

  @media (min-width: 768px) {
    ::-webkit-scrollbar {
  width: 10px;
}

/* Track */
::-webkit-scrollbar-track {
  background: rgba(19,21,26,1);
}

/* Handle */
::-webkit-scrollbar-thumb {
  background:#21222A;
  border-radius: 5px;
}

/* Handle on hover */
/* ::-webkit-scrollbar-thumb:hover {
  background: #555;
} */

.moveUp{
  animation: ${moveUp} 0.5s 0s ease-in forwards;
}
}`;

export default GlobalStyle;
