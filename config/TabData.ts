export const FEATURE_DATA = [
  {
    title: 'BREED',
    imgName: 'Breed.png',
    description: 'You can create a new generation of pets by breeding any two pets.',
    link: 'https://mydefipet.medium.com/my-defi-pet-breed-5365afaae9cc',
  },
  {
    title: 'EVOLVE',
    imgName: 'Picture_Evolve.png',
    description: 'A pet has many stages of evolution depending on the Rarity.',
    link: 'https://mydefipet.medium.com/my-defi-pet-tutorial-how-to-play-ab13137fac37',
  },
  {
    title: 'WORLD BOSS',
    imgName: 'SplashArt_Boss_Hydra.jpg',
    description: 'Team up your pets to hunt down World Boss for numerous rewards include DPET.',
    link: 'https://mydefipet.medium.com/boss-fight-new-update-5323b61aa518',
  },
  {
    title: 'ARENA - PVP',
    imgName: 'PetBattles_Nologo.jpg',
    description: 'Face up others players around the world to compete seasonal rewards.',
    link: 'https://mydefipet.medium.com/-a529a9f58454',
  },
  {
    title: 'MARKETPLACE',
    imgName: 'Banner_marketplace_launch.jpg',
    description: 'Trade, auction for rare pets and make your fortune.',
    link: 'https://mydefipet.medium.com/-ea7d261e580d',
  },
  {
    title: 'STAKING',
    imgName: 'Picture_Staking.png',
    description: ' Become part of the game by staking DPET tokens into pools.',
    link: 'https://mydefipet.medium.com/-fc9b79a5012f',
  },
  {
    title: 'GOVERNANCE',
    imgName: 'Governence.png',
    description: 'Vote for new features and settings of the game to earn special rewards.',
    link: 'https://mydefipet.medium.com/-fc9b79a5012f',
  },
];

export const TOKEN_ECONOMIC_DATA = [];

export const TEAM_DATA = [
  {
    personName: 'TRI PHAM',
    title: 'Chief Executive Officer',
    description: ['Co-Founder & CEO @KardiaChain'],
    avatar_name: 'Avatar_Tri_Pham.png',
  },
  {
    personName: 'JOHN HUY NGUYEN',
    title: 'Chief Business Officer',
    description: ['Head of Business Development @KardiaChain', 'Program Manager @Bayer'],
    avatar_name: 'Avatar_John_Huy_Nguyen.png',
  },
  {
    personName: 'TOMMY LE',
    title: 'Game Marketer',
    description: ['Chief Operating Officer @Topebox', 'Game Producer @Gameloft', 'Head @Firebat Studio (VNG)'],
    avatar_name: 'Avatar_Tommy_Le.png',
  },
  {
    personName: 'ANH TRAN',
    title: 'GAME DEVELOPER',
    description: ['Chief Gaming Officer @Topebox', 'Game Product Manager @VNG', 'Product Manager @Topebox'],
    avatar_name: 'Avatar_Anh_Tran.png',
  },
  {
    personName: 'LIEM THAI',
    title: 'Advisor',
    description: ['CEO @Topebox', 'Product Lead @Gameloft', 'Co-founder @Firebat Studio (VNG)'],
    avatar_name: 'Avatar_Liem Thai.png',
  },

  {
    personName: 'TIEP VU',
    title: 'ADVISOR',
    description: [
      'Game Growth Hacker',
      'Business Development Director @Topebox',
      'Co-founder @Gamebank (game payment)',
      'Co-founder @Horus (VR gaming)',
    ],
    avatar_name: 'Avatar_Tiep_Vu.png',
  },
  {
    personName: 'JEROME WONG',
    title: 'ADVISOR',
    description: [
      'Co-founder & CBO, Everest Ventures Group (EVG)',
      'Wong advises Animoca’s blockchain game business and serves as advisor for several other projects such as Flow, Sandbox, Gamee, Lightnet, HKbitex. He has raised over $60M for projects.',
    ],
    avatar_name: 'Avatar_Wong.png',
  },
  {
    personName: 'WAYNE LIN',
    title: 'ADVISOR',
    description: [
      'Founder, Axia8 Ventures',
      'Wayne is an experienced blockchain consultant and advisor for many renowned, successfully launched projects, such as Solv. Protocol, Kickpad, Dorafactory, Injective Protocol, Kira Network, Findora.',
    ],
    avatar_name: 'Avatar_Lin.png',
  },
];

export const PET_DATA = [
  {
    petName: 'Spike',
    element: 0,
    description1: 'Fate forces me to become an assassin.',
    description2:
      'Legend has it that an ancient creature existed thousands of years ago. No one has ever encountered this creature face to face. Only a few saw its shadow as the myth about this creature is spread among villagers from generations to generations.',
    stage1ImgPath: 'spike_st1.png',
    stage2ImgPath: 'spike_st2.png',
    avatarImgPath: 'spike.png',
    customWidth: null,
    customHeight: null,
    customWidthStage2: null,
    customHeightStage2: null,
  },
  {
    petName: 'Adonis',
    element: 3,
    description1: 'as',
    description2:
      'Unlike other My Defi Pet creatures, Adonis is the most human-friendly ones. Loyal, courage and protective are the characteristics that come to mind when talking about Adonis.',
    stage1ImgPath: 'adonis_st1.png',
    stage2ImgPath: 'adonis_st2.png',
    avatarImgPath: 'adonis.png',
    customWidth: null,
    customHeight: null,
    customWidthStage2: null,
    customHeightStage2: null,
  },
  {
    petName: 'Winged Pupper',
    element: 2,
    description1: 'zz',
    description2:
      'Known as the child of the wind, it has an incredible speed in the MDP world, which is difficult for any other creatures to catch up with. The nemesis of the Spike, they have fought each other for thousands of years before humans appeared and tamed them.',
    stage1ImgPath: 'puper_st1.png',
    stage2ImgPath: 'puper_st2.png',
    avatarImgPath: 'puper.png',
    customWidth: null,
    customHeight: null,
    customWidthStage2: null,
    customHeightStage2: null,
  },
  {
    petName: 'Tygra',
    element: 3,
    description1: 'cccccc',
    description2:
      'Born from the most isolated land, Tygra is a very difficult creature to find, it only appears to normal eyes whenever it feels like to. Tygra embodies the Fire element, which is useful in both attack and defensive maneuvers',
    stage1ImgPath: 'tygra_st1.png',
    stage2ImgPath: 'tygra_st2.png',
    avatarImgPath: 'tygra.png',
    customWidth: null,
    customHeight: null,
    customWidthStage2: null,
    customHeightStage2: null,
  },
  {
    petName: 'Bugsy',
    element: 2,
    description1: 'ff',
    description2:
      'Being one of the giant bugs of this world, Bugsy is very tender in nature. Notice not to poke it, with the power of water it is hard to imagine how powerful Bugsy really is.',
    stage1ImgPath: 'bugsy_st1.png',
    stage2ImgPath: 'bugsy_st2.png',
    avatarImgPath: 'bugsy.png',
    customWidth: null,
    customHeight: null,
    customWidthStage2: null,
    customHeightStage2: null,
  },
  {
    petName: 'Fang',
    element: 1,
    description1: 'ccff',
    description2:
      'Mighty Fang is a relative of Adonis. Unlike him, Mighty Fang absorbs the essence of the earth element from this magical world to protect its friends and allies against all forces trying to harm them.',
    stage1ImgPath: 'fang_st1.png',
    stage2ImgPath: 'fang_st2.png',
    avatarImgPath: 'fang.png',
    customWidth: null,
    customHeight: null,
    customWidthStage2: null,
    customHeightStage2: null,
  },

  {
    petName: 'Rudolph',
    element: 1,
    description1: 'Fate forces me to become an assassin.',
    description2:
      'Rudolph represents wisdom and knowledge with unparalleled composure. As Rudolph is one of the eldest creatures in the world of My Defi Pet, locals regard it as a divine entity and worship it since the dawn of time. Embracing the power of nature, Rudolph brings balance and harmony to this land.',
    stage1ImgPath: 'rudolph_st1.png',
    stage2ImgPath: 'rudolph_st2.png',
    avatarImgPath: 'rudolp.png',
    customWidth: null,
    customHeight: null,
    customWidthStage2: null,
    customHeightStage2: null,
  },

  {
    petName: 'Venom',
    element: 2,
    description1: 'Fate forces me to become an assassin.',
    description2:
      'Venom is the most mysterious and venomous creature in My DeFi Pet. Poison runs in the veins and bubbles in the guts, Venom sows fear and nightmares upon this tender land. ',
    stage1ImgPath: 'venom_st1.png',
    stage2ImgPath: 'venom_st2.png',
    avatarImgPath: 'venom.png',
    customWidth: null,
    customHeight: null,
    customWidthStage2: null,
    customHeightStage2: null,
  },

  {
    petName: 'Pandaren',
    element: 5,
    description1: 'Fate forces me to become an assassin.',
    description2:
      'In the aeon after the birth of the Light, Pandaren was the shining herald of order and justice. As darkness approached, Pandaren vowed to fight to restore the Light to this world.',
    stage1ImgPath: 'panda_st1.png',
    stage2ImgPath: 'panda_st2.png',
    avatarImgPath: 'panda.png',
    customWidth: null,
    customHeight: null,
    customWidthStage2: null,
    customHeightStage2: null,
  },
  {
    petName: 'Ao Shun',
    element: 4,
    description1: 'Fate forces me to become an assassin.',
    description2:
      'Behold the mighty Lord of Darkness, Ao Shun ruled for untold years. Such is his evil, so intense his presence, that no sanity one dare to oppose him.',
    stage1ImgPath: 'aoshun_st1.png',
    stage2ImgPath: 'aoshun_st2.png',
    avatarImgPath: 'aoshun.png',
    customWidth: null,
    customHeight: null,
    customWidthStage2: null,
    customHeightStage2: null,
  },
  {
    petName: 'Spike V2',
    element: 0,
    description1: 'Fate forces me to become an assassin.',
    description2:
      'Legend has it that an ancient creature existed thousands of years ago. No one has ever encountered this creature face to face. Only a few saw its shadow as the myth about this creature is spread among villagers from generations to generations.',
    stage1ImgPath: 'New_Spike_baby.PNG',
    stage2ImgPath: 'New_Spike_adult.PNG',
    avatarImgPath: 'spike_new.png',
    customWidth: null,
    customHeight: null,
    customWidthStage2: null,
    customHeightStage2: null,
  },
  {
    petName: 'Adonis V2',
    element: 3,
    description1: 'as',
    description2:
      'Unlike other My Defi Pet creatures, Adonis is the most human-friendly ones. Loyal, courage and protective are the characteristics that come to mind when talking about Adonis.',
    stage1ImgPath: 'New_Adonis_baby.PNG',
    stage2ImgPath: 'New_Adonis_Adult.PNG',
    avatarImgPath: 'adonis_new.png',
    customWidth: null,
    customHeight: null,
    customWidthStage2: null,
    customHeightStage2: null,
  },
  {
    petName: 'Winged Pupper V2',
    element: 2,
    description1: 'zz',
    description2:
      'Known as the child of the wind, it has an incredible speed in the MDP world, which is difficult for any other creatures to catch up with. The nemesis of the Spike, they have fought each other for thousands of years before humans appeared and tamed them.',
    stage1ImgPath: 'New_WingedPupper_Baby.PNG',
    stage2ImgPath: 'New_WingedPupper_Adult.png',
    avatarImgPath: 'puper_new.png',
    customWidth: null,
    customHeight: null,
    customWidthStage2: null,
    customHeightStage2: null,
  },
  {
    petName: 'Tygra V2',
    element: 3,
    description1: 'cccccc',
    description2:
      'Born from the most isolated land, Tygra is a very difficult creature to find, it only appears to normal eyes whenever it feels like to. Tygra embodies the Fire element, which is useful in both attack and defensive maneuvers',
    stage1ImgPath: 'New_Tygra_Baby.PNG',
    stage2ImgPath: 'New_Tygra_Adult.PNG',
    avatarImgPath: 'tygra_new.jpg',
    customWidth: null,
    customHeight: null,
    customWidthStage2: null,
    customHeightStage2: null,
  },
  {
    petName: 'Bugsy V2',
    element: 2,
    description1: 'ff',
    description2:
      'Being one of the giant bugs of this world, Bugsy is very tender in nature. Notice not to poke it, with the power of water it is hard to imagine how powerful Bugsy really is.',
    stage1ImgPath: 'bugzy.gif',
    stage2ImgPath: 'New_Bugsy_Adult.PNG',
    avatarImgPath: 'bugsy_new.png',
    customWidth: null,
    customHeight: null,
    customWidthStage2: null,
    customHeightStage2: null,
  },
  {
    petName: 'Fang V2',
    element: 1,
    description1: 'ccff',
    description2:
      'Mighty Fang is a relative of Adonis. Unlike him, Mighty Fang absorbs the essence of the earth element from this magical world to protect its friends and allies against all forces trying to harm them.',
    stage1ImgPath: 'New_Fang_baby.PNG',
    stage2ImgPath: 'New_Fang_adult.PNG',
    avatarImgPath: 'fang_new.png',
    customWidth: null,
    customHeight: null,
    customWidthStage2: null,
    customHeightStage2: null,
  },
  {
    petName: 'Rudolph V2',
    element: 1,
    description1: 'Fate forces me to become an assassin.',
    description2:
      'Rudolph represents wisdom and knowledge with unparalleled composure. As Rudolph is one of the eldest creatures in the world of My Defi Pet, locals regard it as a divine entity and worship it since the dawn of time. Embracing the power of nature, Rudolph brings balance and harmony to this land.',
    stage1ImgPath: 'New_Rudolph_Baby.PNG',
    stage2ImgPath: 'New_Rudolph_Adult.PNG',
    avatarImgPath: 'rudolp_new.png',
    customWidth: null,
    customHeight: null,
    customWidthStage2: 500,
    customHeightStage2: 600,
  },
  {
    petName: 'Venom V2',
    element: 2,
    description1: 'Fate forces me to become an assassin.',
    description2:
      'Venom is the most mysterious and venomous creature in My DeFi Pet. Poison runs in the veins and bubbles in the guts, Venom sows fear and nightmares upon this tender land. ',
    stage1ImgPath: 'New_Venom_Baby.PNG',
    stage2ImgPath: 'New_Venom_Adult.PNG',
    avatarImgPath: 'venom_new.png',
    customWidth: null,
    customHeight: null,
    customWidthStage2: null,
    customHeightStage2: null,
  },
];
