export const APP_NAME = process.env.NEXT_PUBLIC_APP_NAME || 'My Defi Pet';
export const ACCOUNT_PAGE = process.env.NEXT_PUBLIC_ACCOUNT_PAGE || 'https://ark-rivals-web-account.pages.dev/';
export const APP_DESCRIPTION =
  process.env.NEXT_PUBLIC_APP_DESCRIPTION ||
  'My DeFi Pet  is a virtual pet game that combines DeFi, collectibles and your own personality. My DeFi Pet is operated on Supported Network including Binance Smart Chain and KardiaChain.';
export const APP_SITE_URL = process.env.NEXT_PUBLIC_APP_SITE_URL || 'http://localhost:3000';
export const BEP20_TOKEN_ADDRESS = '0xfb62ae373aca027177d1c18ee0862817f9080d08';
export const KRC20_TOKEN_ADDRESS = '0xfb62ae373aca027177d1c18ee0862817f9080d08';
export const BG_NAME = ['', 'Background_01.png', 'Background_02.jpg', 'Background_03.png', 'Background_04.png'];
export const PLAY_URL = 'https://play.mydefipet.com/';
export const BEP20_CONTRACT_URL = 'https://bscscan.com/token/0xfb62ae373aca027177d1c18ee0862817f9080d08';
export const KRC20_CONTRACT_URL =
  'https://explorer.kardiachain.io/address/0xfb62AE373acA027177D1c18Ee0862817f9080d08/transactions';
export const BUY_TOKEN_URL =
  'https://pancakeswap.finance/swap?inputCurrency&outputCurrency=0xfb62ae373aca027177d1c18ee0862817f9080d08';

export const VERSION = 'v0.8';
export const GOOGLE_CLIENT_EMAIL = 'billyct@mydefipet-subscribe.iam.gserviceaccount.com';
export const GOOGLE_PRIVATE_KEY =
  '-----BEGIN PRIVATE KEY-----\nMIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCjgij/s2nW8c8Q\nskY+0XqcUavItdp4Z7fvRNwkxFeIpeXq/N9RQueNr8s2AYSQnQsoadWnVyrAFj/S\n+88IecAzIPxk3lestw/YFBCd/h4J/g6FAKcCPkm8/HnzCYpjtGn0XVla7fgNXfA3\npLcGWlxwgX10CEA3NFLn9TwctkoNMlJCynpbhxs2xnXfxmcUsAF1pXHCWUS/n8/o\nZSRHnlxKoOXRHiTecv7MTY0AgtqOVunIzCVIseiWj8qKEgJ4e6l3h9yLTh/l1/sE\nloCPNNSj4oW6WcogV461Up35O4WmaTGHfS1/LsZ2HcSyZsEQpzTZaVmA7hV7+xwS\nQGmCovFpAgMBAAECggEAGWHDtonl5LGN2MQr5mWDWI/PEG22d9l18qUoXWbDRdmm\n+tb9XbJ6moKPE5ehugc+66ueOIduAfqSNtDTHzgzL1ywAYKIbq3OPldDCxufEyMG\novvqcQ01SiqRTbqN6pNfnUj+bYCicITEHQcrSEevsOFRFbmQKmGrewmVJsnfbfPB\nv+OqByHebmD+bWjMXPBt+4fdGbt8NJIG/pvfekhNk9mwvp+eOBaDfUzY/KazX62X\n6/KIBqfH5rbU/2g0Nf7bTp1x5ehEUHxOUmr2zWpnotBKQ1QcDRhOCJU78ZdCCPJ4\nKgQkt4UPDuxikpWKM0IKrijiRovZ1967qCQ1OoCPbwKBgQDVqZZbF0RADO58dqwV\nc17wfB6Z71XafGoAD4FQeiQJveaBpSjjIAXdNnYYvnrbXovxIecW7fXzZyHuIMKF\n2Ftr6s5rsoHvtAjdbFcYDnQl/bDm5VARSuD3hKd5wlb4J05ZFcmbtwaPF+Fi//E2\n5+euMgRPgUWiuqkTzaeynbOqnwKBgQDD6GuSYb4hTNxE/cZxPfOZSTKuQ7SeD14i\nd8Zr9j+BqhMQlWI0rcmUYrBBQHDz6qHNKyRrEHgg20Ok3qR18Iivp2dh8PaB7ns2\nieJ/1RuYqdgpnjXJQqnbmYSKRODp11Cqz4cwGyBjdHGqTpH0YQhZuDPFTeaEQDFE\n063a32du9wKBgQDLgcn6XUDemPKWjhn+HHNzL2WeTT5C6nm0MRCEbLHirYPePEej\nfZ7YQsVdcCBozbcRJ6+KQYEgIz1IKN/eyGLbeTy/i7kDbUF8VXLnMO0kOmzCn+wR\nbHa2ix1Rde+MUTPo2j27GSsgJCjDOPCx0VT0sCM27vQ6uPvOJvXxxQ+aFQKBgBoU\nEl3GDR05sgitD4F7kbXR/KEd+ikgOpO0BgHRIdsM3MTUXeJNUAwkpvnIrkCJ90fs\nekiWRKd7CKIl0RayEj/vtN9gyC+7o0M7T59cqvPDiDbiDJ7h9OJ6qR4O1THLq71R\nKtnIgqM6mPbmxp8VW5s13g5htFHeibBzzSzZmFc5AoGAaiRQ+D/UYX2vbhHYEcJo\nDOME3a680vDaHJzE5NFLX3pc/EBYyk+MprzTTGuxkVCbzchXmn0Y01xTc9DKc4T/\nSgaf7mk84PgLVxfySq0tg2p7DWHw22JNT4CqBx0B7ye0FX2UiaiLErtHbIGzWH4H\nTASytvE9RCBiPOM+SoLNTNI=\n-----END PRIVATE KEY-----\n';
export const GOOGLE_SHEET_ID = '1z9eYree8tbiFIC2EZq-8NoAdsSgbRPr_amYOGlnnucw';
export const API_KEY = 'AIzaSyBOcFfKMt6MmTaAkyWQT7QXxJ0pF-OpZ1M';
