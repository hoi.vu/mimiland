import { DefaultTheme } from 'styled-components';

const lightTheme: DefaultTheme = {
  mode: 'light',
  colors: {
    transparent: 'transparent',
    background: {
      default: '#fafafa',
      box: '#fff',
    },
    text: {
      primary: 'rgba(0, 0, 0, 0.87)',
      secondary: 'rgba(0, 0, 0, 0.54)',
      disabled: 'rgba(0, 0, 0, 0.38)',
    },
    divider: 'rgba(0, 0, 0, 0.12)',
  },
};

export default lightTheme;
